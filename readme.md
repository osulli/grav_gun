# Grav Gun

Simple grav gun with mass check.

## Key blueprints:

```
Blueprint'/Game/FirstPersonBP/Blueprints/FirstPersonCharacter.FirstPersonCharacter'
Blueprint'/Game/grav_gun/blueprints/grav_gun_comp.grav_gun_comp'
```

## Setup


To your player Character BP:

- Add grav_gun_comp as component, ensure `auto_activate = true`

- Add a cube that has `hidden_in_game = true` with the following transform:

```
260, 0, 40
0, 0, 0
0.1, 0.1, 0.1
```

- Add a PhysicsHandle to your player BP with default settings

- Add the following:

![my_character_setup.png](Images/my_character_setup.png)

## Demo

![demo.gif](Images/demo.gif)
